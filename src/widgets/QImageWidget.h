/*
 * QImageWidget.h
 *
 *  Created on: Jan 20, 2014
 *      Author: OlegKarpov
 */

#ifndef QIMAGEWIDGET_H_
#define QIMAGEWIDGET_H_

#include <QtGui>
#include <list>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

QImage Mat2QImage(cv::Mat const& src);

class QImageWidget: public QWidget
{
	Q_OBJECT

public:
	QImageWidget(QWidget* parent);
	int setImage(QImage& image);
	int addImage(QImage& image);

protected:
	void paintEvent(QPaintEvent * event);

private:
	QImage mImage;
	std::list<QImage> mShotList;
	bool should_repaint;
};


#endif /* QIMAGEWIDGET_H_ */
