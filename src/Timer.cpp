#include "Timer.h"
#include "stdio.h"

Timer::Timer(std::string name)
:mElapsed(0.)
,mDelta(0.)
{
}

double Timer::GetElapsed()
{
	return mElapsed;
}

double Timer::GetDelta()
{
	return mDelta;
}

void Timer::CountElapsed(double t)
{
	mElapsed += t;
}

void Timer::ResetElapsed()
{
	mElapsed = 0.;
}

void Timer::SetDelta(double t)
{
	mDelta = t;
}

int Timer::Reset()
{
	mElapsed = 0.;
	mDelta = 0.;
}

int Timer::Log()
{
	fprintf(stderr, "%s:%s:mElapsed:%f\n", __FILE__, __FUNCTION__, mElapsed);
	fprintf(stderr, "%s:%s:mDelta:%f\n", __FILE__, __FUNCTION__, mDelta);

	return 0;
}
