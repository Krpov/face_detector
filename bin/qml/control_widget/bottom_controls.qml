import QtQuick 1.0
import "button"
import "progress"

Rectangle {
    id: bottom_widget
    objectName: "bottom_widget"
    
    //y: parent.height/4*3
    //height: parent.height/4
    //width: parent.width
    color: Qt.rgba(0.,0.,0.,0.5)
     //opacity: 0.2
    state: 'visible'
     	 
    property int y_prop: y
     	   
    property int button_field: width - width*0.4
    property int button_and_spacing: button_field*0.2
    property int button_size: button_and_spacing*0.6
    property int buton_spacing: button_and_spacing*0.4
    property int margin: (width - button_field + buton_spacing)/2
     	    
    gradient: Gradient {
        GradientStop { position: 0.0; color: Qt.rgba(0.0,0.0,0.3,0.4) }
        GradientStop { position: 1.0; color: Qt.rgba(0.,0.5,0.6,1.0) }
    }
    
    Button {
    	id: back_button
    	objectName: "back_button"
    	
        x: bottom_widget.margin
		y: parent.height/2 - bottom_widget.button_size/2
    	height: bottom_widget.button_size
    	width: height

		normal_image: "../images/controls11/begin.png"
		
		image_count: 1
		image_urls: ["../images/controls11/begin.png"]
	}
	
	Button {
		id: backward_button
    	objectName: "backward_button"
		
		x: back_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
		y: parent.height/2 - bottom_widget.button_size/2
    	height: bottom_widget.button_size
    	width: height

		normal_image: "../images/controls11/backward.png"
		
		image_count: 1
		image_urls: ["../images/controls11/backward.png"]
	}
	
	Button {
		id: play_button
    	objectName: "play_button"
		
		x: backward_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
		y: parent.height/2 - bottom_widget.button_size/2
    	height: bottom_widget.button_size
    	width: height

		normal_image: "../images/controls/play.png"
		
		image_count: 2
		image_urls: ["../images/controls11/play.png", "../images/controls11/pause.png"]
	}	
	
	Button {
		id: forward_button
    	objectName: "forward_button"
		
		x: play_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
		y: parent.height/2 - bottom_widget.button_size/2
    	height: bottom_widget.button_size
    	width: height

		normal_image: "../images/controls11/forward.png"
		
		image_count: 1
		image_urls: ["../images/controls11/forward.png"]
	}	
	
	Button {
		id: end_button
    	objectName: "end_button"
		
		x: forward_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
		y: parent.height/2 - bottom_widget.button_size/2
    	height: bottom_widget.button_size
    	width: height

		normal_image: "../images/controls11/end.png"
		
		image_count: 1
		image_urls: ["../images/controls11/end.png"]
	}	
	
	Button {
		id: record_button
    	objectName: "record_button"
		
		x: bottom_widget.width - bottom_widget.margin/2
		y: parent.height/2 - bottom_widget.button_size/2 + height/2
    	height: bottom_widget.button_size/2
    	width: height

		normal_image: "../images/controls11/record.png"
		
		image_count: 1
		image_urls: ["../images/controls11/record.png"]
	}	
	
	Button {
    	id: stop_button
    	objectName: "stop_button"
    	
        x: record_button.x - bottom_widget.margin/4
        y: parent.height/2 - bottom_widget.button_size/2 + height/2	    	
    	height: bottom_widget.button_size/2
    	width: height
    	
    	//s5, s6, s7, s8, s9
		normal_image: "../images/controls11/stop.png"
		
		image_count: 1
		image_urls: ["../images/controls11/stop.png"]
	}
	
	Progress {
		id: time_progress
	    objectName: "time_progress"
		
		property int offset:bottom_widget.width*0.05
		
		x: offset
		y: (back_button.y - bottom_widget.y)/2
		width: bottom_widget.width - offset*2
		height: bottom_widget.height*0.05
		
		traveller_source: "../control_widget/images/sphere/s1.png"
	}
	
	function switchState() {
         console.log(objectName, ":switchState:", state);//, " state: ", state);
          
         if(state == 'visible')
         {
         	state = 'invisible'
         }
         else
         {
         	state = 'visible'
         }
    }
    
    states: [
        State {
            name: "visible"
            PropertyChanges { target: bottom_widget; opacity: 1.0 }
        },

        State {
            name: "invisible"
            PropertyChanges { target: bottom_widget; opacity: 0.0 }
        }
    ]
    

    transitions: [

        Transition {
            from: "*"; to: "visible"
            NumberAnimation { properties: "opacity"; duration: 200 }
        },

        Transition {
            from: "*"; to: "invisible"
            NumberAnimation { properties: "opacity"; duration: 200 }
        },

        Transition {
            NumberAnimation { properties: "opacity"; duration: 200 }
        }
    ]
}