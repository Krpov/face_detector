/****************************************************************************
**
** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.0

Rectangle {
    id: progress_layout
    
    signal progressDragged(real shift)
    
    property alias traveller_source: traveller.source
    property bool dragging: false
    property real currentPosition: 0.
           
    color: Qt.rgba(0.,0.,0.,0.0)
     //opacity: 0.2
    radius: 50
    
    border.color: Qt.rgba(0.3,0.3,0.,0.3)
    border.width: 1
    
    gradient: Gradient {
        GradientStop { position: 0.0; color: Qt.rgba(0.3,0.3,0.3) }
        GradientStop { position: 1.0; color: Qt.rgba(0.,0.,0.0,0.0) }
    }
    
    Rectangle {
    	id: progress
    	objectName: "progress"
    	    	
    	//onWidthChanged:
	    //{
	    //    console.log("progress:onWidthChanged: ", width);
	    //}
    	    	
    	x: 0
    	y: 0
    	
    	width: parent.width*progress_layout.currentPosition
    	height: parent.height
    	
    	radius: 50
     
	    gradient: Gradient {
	        GradientStop { position: 0.0; color: Qt.rgba(0.8,0.8,0.8) }
	        GradientStop { position: 1.0; color: Qt.rgba(0.,0.,0.0,0.0) }
	    }	
	    
	    BorderImage {
	    	id: traveller
	    	objectName: "traveller"
	    	
	    	x: progress.width - width/2
	    	y: - height/2 + parent.height/2
	    	height: parent.height*3
	    	width: height
	    	
	    	opacity: .6
	    } 	    
    } 
        
    function drag_progress(x)
    {
        console.log("drag_progress: x: ", x);

    	if(x >= progress_layout.width)
    	{
    		progress.width = progress_layout.width;
    	}
    	else if(x < 1)
    	{
    		progress.width = 0;
    	}
    	else
    	{
    		progress.width = x;
    	}
    	
    	traveller.x = progress.width - traveller.width/2;
    }
        
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onPositionChanged: {        	
        	drag_progress(mouse.x)
        	//console.log("onPositionChanged: mouse: ", mouse.x, ", ", mouse.y, " width: ", parent.width, " dragging: ", progress_layout.dragging);
        	//progressDragged(progress.width/parent.width)
        }      
        
        onPressed:
        {        	
        	progress_layout.dragging = true;
        	drag_progress(mouse.x)  
        	//console.log("onPressed: mouse: ", mouse.x, ", ", mouse.y, " width: ", parent.width, " dragging: ", progress_layout.dragging);
        }
        
        onReleased: {
            progress_layout.dragging = false;        	
        	console.log("onReleased: mouse: ", mouse.x, ", ", mouse.y, " width: ", parent.width, " dragging: ", progress_layout.dragging);
        	
        	progressDragged(traveller.x/parent.width)
        }
    }
    	        
     //function myQmlFunction(cuurent_pos) {
     //    console.log("Got message: ", cuurent_pos)
     //    
     //    if(traveller)
     //    traveller.x = 100;//progress.x + progress.width - traveller.width/2
     //}
    
    onCurrentPositionChanged:
    {
    	if(!progress_layout.dragging)
    	{
        	console.log("progress_layout:onCurrentPositionChanged: ", currentPosition, ", width:", parent.width);
        	
        	progress.width = progress_layout.width*progress_layout.currentPosition
        	traveller.x = progress.width - traveller.width/2
        }
    }
    
} 
	