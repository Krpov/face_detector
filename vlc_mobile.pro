QT += gui declarative 

CONFIG += console

DESTDIR = bin
OBJECTS_DIR = obj
MOC_DIR = moc
RCC_DIR = rcc
UI_DIR = ui
SRC_DIR = ./src
LIB_DIR = ./lib
RESOURCES = $${DESTDIR}/rc.qrc

INCLUDEPATH += $${SRC_DIR} $${LIB_DIR} $${SRC_DIR}/widgets $${SRC_DIR}/data /usr/local/include

LIBS += /usr/local/lib/libopencv_objdetect.so /usr/local/lib/libopencv_core.so /usr/local/lib/libopencv_imgproc.so /usr/local/lib/libopencv_highgui.so

HEADERS     = 	$${SRC_DIR}/widgets/video_widget.h \
				$${SRC_DIR}/Timer.h \
				$${SRC_DIR}/PosixTimer.h \
				$${SRC_DIR}//widgets/QImageWidget.h \
              	$${SRC_DIR}/widgets/control_widget.h \
              	$${SRC_DIR}/widgets/main_widget.h
              
SOURCES     = $${SRC_DIR}/widgets/video_widget.cpp \
			  $${SRC_DIR}/Timer.cpp \
			  $${SRC_DIR}/PosixTimer.cpp \			  
			  $${SRC_DIR}//widgets/QImageWidget.cpp \
			  $${SRC_DIR}/widgets/control_widget.cpp \
			  $${SRC_DIR}/widgets/main_widget.cpp \
			  $${SRC_DIR}/main.cpp			
			  #$${SRC_DIR}/play_video.cpp 
